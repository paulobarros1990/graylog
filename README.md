<img src = "img/Graylog-logo-blk.jpg">

> Status: Produção

### Descrição Executiva

A Gestão atual da CGETI no exercício de suas atribuições está na busca constante pela sintonia com as boas práticas de mercado e pelo alinhamento com as orientações dos órgãos de controle. Para tanto, fez-se necessária à identificação da situação atual e de um plano de ação para evoluir a gestão de logs feitas hoje no CNPq. Visando a realização desta demanda, a CGETI solicitou à Empresa parceira AlgarTech que utilizasse seu corpo técnico para a produção deste relatório.

## Tecnologias Usadas
<table>
  <tr>
     <td>Graylog</td>
	 <td>MongoDB</td>
     <td>ElasticSearch</td>
  </tr>
  <tr>
     <td>4.2</td>
	 <td>4.2</td>
   <td>7.10</td>   
  </tr>	 
</table>

## Topologia

<img src = "img/infra_graylog.jpg">

## Instalação

Foi realizada a instalação padrão conforme a documentação

> https://docs.graylog.org/v1/docs/centos)

<img src = "img/install_1.JPG">
<img src = "img/install_2.JPG">
<img src = "img/install_3.JPG">
<img src = "img/install_4.JPG">

## Arquivos de Configuração

> /etc/graylog/server/server.conf

> /etc/elasticsearch/elasticsearch.yml

> /etc/mongod.conf

## Backup

Os arquivos de configuração estão no git

> MongoDB - backup feito via Crontab e salvo no NFS.

> Graylog Server - Arquivo de Configuração salvo no git

> ElasticSearch - Snapshot do Storage.

